package datastructure;

import java.awt.Color;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int columns;
	private int rows;
	private CellState[][] cellState;

	
	

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.columns = columns;
    	this.rows = rows;
    	cellState = new CellState[rows][columns];
    	
    	for (int row = 0; row < rows; row++) {
    		for (int col = 0; col < columns; col++) {
    			cellState[row][col] = initialState;
    		}
    	}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	cellState[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
    	
    	Color cellColor = cellState[row][column].getColor();
    	
    	if (cellColor == Color.BLACK) {
    		return CellState.ALIVE;
    	}
    	else if (cellColor == Color.PINK) {
    		return CellState.DYING;
    	}
    	else {
    		return CellState.DEAD;
    	}
    }

    @Override
    public IGrid copy() {
    	IGrid copyOfGrid = new CellGrid(rows, columns, CellState.DEAD);
    	
    	for (int row = 0; row < rows; row++) {
    		for (int col = 0; col < columns; col++) {
    			copyOfGrid.set(row, col, this.get(row, col)); 
    		}
    	}
        return copyOfGrid;
    }
    
}
